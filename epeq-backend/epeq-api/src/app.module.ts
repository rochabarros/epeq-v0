import { Module } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import { OncologiaModule } from './oncologia/oncologia.module';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [ConfigModule, OncologiaModule, DatabaseModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
