import { DatatableParamsDto } from "./datatable-params.dto";

export class DatatableResponseDto {

    data: any[];
    draw: number;
    recordsFiltered: number;
    recordsTotal: number;

    constructor(data: any[], totalRegistros: number, datatableParams: DatatableParamsDto){
        this.data = data;
        this.draw = datatableParams.draw;
        this.recordsTotal = totalRegistros;
        this.recordsFiltered = totalRegistros;
    }
}