export class FiltrosPeriodosDto{
    periodo: string;
    intervalo: string;
    dataInicial: Date;
    dataFinal: Date;
  
    constructor(filtrosPeriodos?: any){
      if(filtrosPeriodos){
        this.periodo = filtrosPeriodos['periodo'];
        this.intervalo = filtrosPeriodos['intervalo'];
      }
    }
  }