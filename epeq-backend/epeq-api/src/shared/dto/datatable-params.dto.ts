export class DatatableParamsDto {

    columns: Array<any>;
    draw: number;
    length: number;
    order: Array<any>;
    search: object;
    start: number;
}