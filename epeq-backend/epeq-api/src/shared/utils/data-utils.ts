import { FiltrosPeriodosDto } from "../dto/filtros-periodos.dto";
import * as moment from 'moment';
import { Periodos } from "../domain/periodos";

export class DateUtils {

    static updateDatasFiltroPeriodo(filtrosPeriodosDto: FiltrosPeriodosDto): void {
        const periodos = Periodos[filtrosPeriodosDto.periodo];
        const datas = DateUtils.extractDatasPeriodo(periodos, filtrosPeriodosDto.intervalo);
        filtrosPeriodosDto.dataInicial = datas['dataInicial'];
        filtrosPeriodosDto.dataFinal = datas['dataFinal'];
    }

    static momentUTC(): moment.Moment {
        return moment().utc();
    }

    static now(): Date {
        return DateUtils.momentUTC().toDate();
    }

    static formatDate(data: Date): string{
        return moment(data).format('YYYY-MM-DD HH:mm:ss')
    }

    static extractDatasPeriodo(periodo: Periodos, intervalo: string): any {
        let dataInicial: Date;
        let dataFinal: Date;
        switch (periodo) {
            case Periodos.HOJE: {
                dataInicial = DateUtils.momentUTC().startOf('day').toDate();
                dataFinal = DateUtils.momentUTC().endOf('day').toDate();
                break;
            }
            case Periodos.SEMANA_ATUAL: {
                dataInicial = DateUtils.momentUTC().startOf('week').toDate();
                dataFinal = DateUtils.momentUTC().endOf('week').toDate();
                break;
            }
            case Periodos.ULTIMOS_SETE_DIAS: {
                dataInicial = DateUtils.momentUTC().subtract(7, 'days').startOf('day').toDate();
                dataFinal = DateUtils.momentUTC().subtract(1, 'days').endOf('day').toDate();
                break;
            }
            case Periodos.ULTIMOS_QUINZE_DIAS: {
                dataInicial = DateUtils.momentUTC().subtract(15, 'days').startOf('day').toDate();
                dataFinal = DateUtils.momentUTC().subtract(1, 'days').endOf('day').toDate();
                break;
            }
            case Periodos.ULTIMOS_TRINTA_DIAS: {
                dataInicial = DateUtils.momentUTC().subtract(30, 'days').startOf('day').toDate();
                dataFinal = DateUtils.momentUTC().subtract(1, 'days').endOf('day').toDate();
                break;
            }
            case Periodos.MES_ATUAL: {
                dataInicial = DateUtils.momentUTC().startOf('month').toDate();
                dataFinal = DateUtils.momentUTC().endOf('month').toDate();
                break;
            }
            case Periodos.INTERVALO: {
                let values = intervalo.split(' - ');
                dataInicial = moment(`${values[0]} 00:00:00`, 'DD/MM/YYYY HH:mm:ss').toDate();
                dataFinal = moment(`${values[1]} 23:59:59`, 'DD/MM/YYYY HH:mm:ss').toDate();
                break;
            }
        }
        return {
            dataInicial: dataInicial,
            dataFinal: dataFinal
        }
    }
}