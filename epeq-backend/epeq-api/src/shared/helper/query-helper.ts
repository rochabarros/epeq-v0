import { FiltrosPeriodosDto } from "../dto/filtros-periodos.dto";
import { DateUtils } from "../utils/data-utils";
import { Periodos } from "../domain/periodos";

export class QueryHelper{

    filters = {};

    constructor(){}

    setPeriodoBetweenCriteria(field: string, filtrosPeriodosDto: FiltrosPeriodosDto): void{
        DateUtils.updateDatasFiltroPeriodo(filtrosPeriodosDto);
        this.setDatasBetweenCriteria(field, filtrosPeriodosDto.dataInicial, filtrosPeriodosDto.dataFinal);
    }

    setIntervaloBetweenCriteria(field: string, intervalo: string): void{
        const datas = DateUtils.extractDatasPeriodo(Periodos.INTERVALO, intervalo);
        this.setDatasBetweenCriteria(field, datas.dataInicial, datas.dataFinal);
    }

    setDatasBetweenCriteria(field: string, dataInicial: Date, dataFinal: Date): void{
        const d1 = DateUtils.formatDate(dataInicial);
        const d2 = DateUtils.formatDate(dataFinal);
        this.filters[field] = { $gte: new Date(d1), $lte: new Date(d2) };
    }

    setLikeCriteria(field: string, value: string): void{
        this.filters[field] = new RegExp('.*' + value + '.*', "i");
    }

    setEqualsCriteria(field: string, value: string): void{
        this.filters[field] = new RegExp(value, "i");
    }
}