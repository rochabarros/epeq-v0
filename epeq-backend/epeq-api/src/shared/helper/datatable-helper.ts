import { DatatableParamsDto } from "../dto/datatable-params.dto";
import * as moment from 'moment';

export class DatatableHelper {

    datatableParams: DatatableParamsDto;

    constructor(datatableParams: DatatableParamsDto) {
        this.datatableParams = datatableParams;
    }

    getSort() {
        return this.datatableParams.order[0];
    }

    getSortColumn() {
        const index = this.getSort()['column'];
        return this.datatableParams.columns[index]['data'];
    }

    getSortType() {
        return this.getSort()['dir'];
    }

    getSortTypeMongo() {
        return this.getSortType() === 'asc' ? 1 : -1;
    }

    getSortMongo() {
        const sort = {};
        sort[this.getSortColumn()] = this.getSortTypeMongo();
        return sort;
    }

    getFilter(): any {
        const filters = {};
        this.datatableParams.columns.forEach(column => {
            if(column['searchable']){
                const columnName = column['data'];
                const val = column['search']['value'];
                if(val && val !== ''){
                    filters[columnName] = val;
                }
            }
        });
        return filters;
    }

    setLikeCriteria(filters: any, property: string): void{
        if(filters[property]){
            filters[property] = new RegExp('.*' + filters[property] + '.*', "i")
        }
    }

    setEqualsCriteria(filters: any, property: string): void{
        if(filters[property]){
            filters[property] = new RegExp(filters[property], "i")
        }
    }
    
    setDateBetweenCriteria(filters: any, property: string): void{
        if(filters[property]){
            let values = filters[property].split(' - ');
            let d1 = moment(`${values[0]} 00:00:00`, 'DD/MM/YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss");
            let d2 = moment(`${values[1]} 23:59:59`, 'DD/MM/YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss");
            filters[property] = { $gte: d1, $lte: d2 } 
        }
    }

    setDataIsNullOrNotNull(filters: any, property: string): void{
        if(filters[property]){
            if (['SIM', 'S', 'TRUE'].indexOf(filters[property].toUpperCase()) != -1) {
                filters[property] = {$ne: null};
            } else if (['NÃO', 'N', 'FALSE'].indexOf(filters[property].toUpperCase()) != -1) {
                filters[property] = null;
            }
        }
    }
}