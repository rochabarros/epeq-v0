import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Oncologia } from './interfaces/oncologia.interface';
import { DatatableParamsDto } from '../shared/dto/datatable-params.dto';
import { DatatableResponseDto } from '../shared/dto/datatable-response.dto';
import { DatatableHelper } from '../shared/helper/datatable-helper';
import { FiltrosPeriodosDto } from '../shared/dto/filtros-periodos.dto';
import { QueryHelper } from '../shared/helper/query-helper';
import { OncologiaDto } from './dto/oncologia.dto';
import { OncologiaHistoricoDto } from './dto/oncologia.historico.dto';
import { CadDispMedicamentoDto } from './dto/cadDispMedicamento.dto';
import { DateUtils } from '../shared/utils/data-utils';
import { OncologiaSituacoes } from './domain/oncologia.situacoes';
import { ResultadoAcaoOncologiaDto } from './dto/resultadoAcaoOncologia.dto';

@Injectable()
export class OncologiaService {

    const

    constructor(
        @InjectModel('Oncologia') private readonly oncologiaModel: Model<Oncologia>,
    ) { }

    async dataTablePesquisa(datatableParamsDto: DatatableParamsDto): Promise<DatatableResponseDto> {
        const datatableHelper: DatatableHelper = new DatatableHelper(datatableParamsDto);
        let filtros = datatableHelper.getFilter();
        datatableHelper.setLikeCriteria(filtros, 'nomeBeneficiario');
        datatableHelper.setLikeCriteria(filtros, 'nomeMedicamento');
        datatableHelper.setEqualsCriteria(filtros, 'situacao');
        datatableHelper.setDateBetweenCriteria(filtros, 'dataSolicitacao');
        datatableHelper.setDateBetweenCriteria(filtros, 'dataAutorizacao');
        datatableHelper.setDateBetweenCriteria(filtros, 'dataDisponibilidade');
        datatableHelper.setDateBetweenCriteria(filtros, 'dataAgendamento');

        const total = await this.oncologiaModel.countDocuments(filtros);
        const oncologias = await this.oncologiaModel.find(filtros, [], {
            limit: datatableParamsDto.length,
            skip: datatableParamsDto.start,
            sort: datatableHelper.getSortMongo()
        });
        return new DatatableResponseDto(oncologias, total, datatableParamsDto);
    }

    async dataTableFluxo(datatableParamsDto: DatatableParamsDto): Promise<DatatableResponseDto> {
        const datatableHelper: DatatableHelper = new DatatableHelper(datatableParamsDto);
        let filtros = datatableHelper.getFilter();
        datatableHelper.setLikeCriteria(filtros, 'nomeBeneficiario');
        datatableHelper.setDataIsNullOrNotNull(filtros, 'dataSolicitacao');
        datatableHelper.setDataIsNullOrNotNull(filtros, 'dataAutorizacao');
        datatableHelper.setDataIsNullOrNotNull(filtros, 'dataDisponibilidade');
        datatableHelper.setDataIsNullOrNotNull(filtros, 'dataAgendamento');

        const total = await this.oncologiaModel.countDocuments(filtros);
        const oncologias = await this.oncologiaModel.find(filtros, [], {
            limit: datatableParamsDto.length,
            skip: datatableParamsDto.start,
            sort: datatableHelper.getSortMongo()
        });
        return new DatatableResponseDto(oncologias, total, datatableParamsDto);
    }

    async getQuantidadeMedicamentos(filtrosPeriodosDto: FiltrosPeriodosDto): Promise<any[]> {
        const queryHelper = new QueryHelper();
        queryHelper.setPeriodoBetweenCriteria('dataSolicitacao', filtrosPeriodosDto)
        const filters = queryHelper.filters;
        const medicamentos = await this.oncologiaModel.aggregate([
            { $match: filters },
            { $group: { _id: "$nomeMedicamento", total: { $sum: 1 } } },
            {
                $sort: {
                    _id: 1
                }
            }
        ])
        return medicamentos;
    }

    async getTotalSituacoes(filtrosPeriodosDto: FiltrosPeriodosDto): Promise<any[]> {
        const queryHelper = new QueryHelper();
        queryHelper.setPeriodoBetweenCriteria('dataSolicitacao', filtrosPeriodosDto)
        const filters = queryHelper.filters;
        const situacoes = await this.oncologiaModel.aggregate([
            { $match: filters },
            { $group: { _id: "$situacao", total: { $sum: 1 } } }
        ])
        return situacoes;
    }

    async obterOncologiaPorId(id: string): Promise<Oncologia> {
        return await this.oncologiaModel.findById(id);
    }

    async cadastrarHistoricoOncologia(id: string, oncologiaHistoricoDto: OncologiaHistoricoDto): Promise<Oncologia> {
        return await this.oncologiaModel.update(
            { "_id": id },
            { $push: { historico: oncologiaHistoricoDto } }
        )
    }

    async atualizarOncologia(id: string, oncologiaDto: OncologiaDto): Promise<Oncologia> {
        await this.oncologiaModel.findByIdAndUpdate(id, oncologiaDto);
        return await this.obterOncologiaPorId(id);
    }

    async disponibilizarMedicamento(cadDispMedicamentoDto: CadDispMedicamentoDto): Promise<ResultadoAcaoOncologiaDto> {
        const resultados = new ResultadoAcaoOncologiaDto('disponibilizar-medicamentos', cadDispMedicamentoDto.medicamento);
        for (let i = 0; i < cadDispMedicamentoDto.solicitacoes.length; i++) {
            let solicitacao = cadDispMedicamentoDto.solicitacoes[i];
            const id = solicitacao['_id'];
            try {
                let historico = new OncologiaHistoricoDto();
                historico.codUsuario = cadDispMedicamentoDto.usuario;
                historico.dataHistorico = DateUtils.now();
                historico.situacao = OncologiaSituacoes.DISPONIBILIZADO;
                await this.cadastrarHistoricoOncologia(id, historico);
                let oncologiaDto = new OncologiaDto();
                oncologiaDto.dataDisponibilidade = cadDispMedicamentoDto.dataDisponibilidade;
                oncologiaDto.situacao = OncologiaSituacoes.DISPONIBILIZADO;
                await this.atualizarOncologia(id, oncologiaDto);
                resultados.resultadosOk.push({ _id: id })
            } catch (error) {
                resultados.resultadosErros.push({ _id: id, message: error.message })
            }
        }
        resultados.totalOk = resultados.resultadosOk.length;
        resultados.totalErros = resultados.resultadosErros.length;
        return resultados;
    }

}
