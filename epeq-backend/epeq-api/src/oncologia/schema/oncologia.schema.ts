import * as mongoose from 'mongoose';

export const OncologiaSchema = new mongoose.Schema({
    codBeneficiario: String,
    nomeBeneficiario: String,
    nomeMedicamento: String,
    numSolicitacao: String,
    numAutorizacao: String,
    dataSolicitacao: Date,
    dataAutorizacao: Date,
    situacao: String,
    dataDisponibilidade: Date,
    dataAgendamento: Date,
    historico: [
        {
            dataHistorico: Date,
            codUsuario: String,
            situacao: String
        }
    ],
},{ collection: 'oncologias' });