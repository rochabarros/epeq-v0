
import { Document } from 'mongoose';

export interface Oncologia extends Document {
    codBeneficiario: string;
    nomeBeneficiario: string;
    nomeMedicamento: string;
    numSolicitacao: string;
    numAutorizacao: string;
    dataSolicitacao: Date;
    dataAutorizacao: Date;
    dataDisponibilidade: Date;
    dataAgendamento: Date;
    situacao: string;
    historico: [
        {
            dataHistorico: Date,
            codUsuario: string,
            situacao: string
        }
    ];
}