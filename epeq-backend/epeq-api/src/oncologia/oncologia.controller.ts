import { Controller, Post, Body } from '@nestjs/common';
import { DatatableParamsDto } from '../shared/dto/datatable-params.dto';
import { OncologiaService } from './oncologia.service';
import { DatatableResponseDto } from '../shared/dto/datatable-response.dto';
import { FiltrosPeriodosDto } from '../shared/dto/filtros-periodos.dto';
import { CadDispMedicamentoDto } from './dto/cadDispMedicamento.dto';
import { ResultadoAcaoOncologiaDto } from './dto/resultadoAcaoOncologia.dto';

@Controller('oncologia')
export class OncologiaController {

    constructor(
        private readonly oncologiaService: OncologiaService
    ){ }

    @Post('/datatable/pesquisa')
    async dataTablePesquisa(@Body() datatableParamsDto: DatatableParamsDto): Promise<DatatableResponseDto>{
        return await this.oncologiaService.dataTablePesquisa(datatableParamsDto);
    }

    @Post('/datatable/fluxo')
    async dataTableFluxo(@Body() datatableParamsDto: DatatableParamsDto): Promise<DatatableResponseDto>{
        return await this.oncologiaService.dataTableFluxo(datatableParamsDto);
    }

    @Post('/kpis/medicamentos/qtde')
    async getQuantidadeMedicamentos(@Body() filtrosPeriodosDto: FiltrosPeriodosDto): Promise<any[]>{
        return await this.oncologiaService.getQuantidadeMedicamentos(filtrosPeriodosDto);
    }

    @Post('/kpis/situacoes/total')
    async getTotalSituacoes(@Body() filtrosPeriodosDto: FiltrosPeriodosDto): Promise<any[]>{
        return await this.oncologiaService.getTotalSituacoes(filtrosPeriodosDto);
    }

    @Post('/disponibilizarMedicamento')
    async disponibilizarMedicamento(@Body() cadDispMedicamentoDto: CadDispMedicamentoDto): Promise<ResultadoAcaoOncologiaDto>{
        return await this.oncologiaService.disponibilizarMedicamento(cadDispMedicamentoDto);
    }
}
