import { OncologiaDto } from "./oncologia.dto";
import { Oncologia } from "../interfaces/oncologia.interface";

export class OncologiaFluxoDto extends OncologiaDto {
    statusSolicitacao: string;
    statusAutorizacao: string;
    statusDisponibilidadeMedicacao: string;
    statusAgendamento: string;

    constructor(data: Oncologia) {
        super();
        this.codBeneficiario = data.codBeneficiario;
        this.nomeBeneficiario = data.nomeBeneficiario;
        this.nomeMedicamento = data.nomeMedicamento;
        this.numSolicitacao = data.numSolicitacao;
        this.numAutorizacao = data.numAutorizacao;
        this.dataSolicitacao = data.dataSolicitacao;
        this.dataAutorizacao = data.dataAutorizacao;
        this.situacao = data.situacao;
        this.dataAgendamento = data.dataAgendamento;
        this.historico = data.historico;
        this.statusSolicitacao = 'Não';
        this.statusAutorizacao = 'Não';
        this.statusDisponibilidadeMedicacao = 'Não';
        this.statusAgendamento = 'Não';
    }
}