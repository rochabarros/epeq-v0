export class ResultadoAcaoOncologiaDto {

    acao: string;
    id: string;
    totalErros: number;
    totalOk:number;
    resultadosOk: any[];
    resultadosErros: any[];

    constructor(acao: string, id: string){
        this.acao = acao;
        this.id = id;;
        this.totalErros = 0;
        this.totalOk = 0;
        this.resultadosErros = [];
        this.resultadosOk = [];
    }
}