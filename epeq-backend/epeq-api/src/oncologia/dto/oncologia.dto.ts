import { OncologiaHistoricoDto } from "./oncologia.historico.dto";

export class OncologiaDto {

    codBeneficiario: string;
    nomeBeneficiario: string;
    nomeMedicamento: string;
    numSolicitacao: string;
    numAutorizacao: string;
    dataSolicitacao: Date;
    dataAutorizacao: Date;
    dataAgendamento: Date;
    dataDisponibilidade: Date;
    situacao: string;
    historico: OncologiaHistoricoDto[];
}