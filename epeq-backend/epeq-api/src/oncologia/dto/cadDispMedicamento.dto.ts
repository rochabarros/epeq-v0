export class CadDispMedicamentoDto{

    medicamento: string;
    solicitacoes: [];
    dataDisponibilidade: Date;
    usuario: string;
}