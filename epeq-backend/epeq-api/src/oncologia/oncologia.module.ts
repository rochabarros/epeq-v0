import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { OncologiaService } from './oncologia.service';
import { OncologiaController } from './oncologia.controller';
import { OncologiaSchema } from './schema/oncologia.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Oncologia', schema: OncologiaSchema }])
  ],
  providers: [OncologiaService],
  controllers: [OncologiaController]
})
export class OncologiaModule {}
