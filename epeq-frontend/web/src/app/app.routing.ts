import { Routes } from '@angular/router';
import { CommonLayoutComponent } from './layout/common-layout.component';
import { AuthenticationLayoutComponent } from './layout/authentication-layout.component';
import { AuthGuard } from './core/auth/auth.guard';

export const AppRoutes: Routes = [
  {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full',
  },
  {
    path: '',
    component: CommonLayoutComponent,
    children: [
      {
        path: 'home',
        canActivate: [
          AuthGuard
        ],
        loadChildren: './pages/home/home.module#HomeModule'
      }
    ]
  },
  {
    path: '',
    component: CommonLayoutComponent,
    children: [
      {
        path: 'oncologia',
        canActivate: [
          AuthGuard
        ],
        loadChildren: './pages/oncologia/oncologia.module#OncologiaModule'
      }
    ]
  },
  {
    path: '',
    component: CommonLayoutComponent,
    children: [
      {
        path: 'administracao',
        canActivate: [
          AuthGuard
        ],
        loadChildren: './pages/administracao/administracao.module#AdministracaoModule'
      }
    ]
  },
  {
    path: '',
    component: AuthenticationLayoutComponent,
    children: [
      {
        path: 'login',
        loadChildren: './pages/login/login.module#LoginModule'
      }
    ]
  }
];

