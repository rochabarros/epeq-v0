import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from './shared/shared.module';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { CommonLayoutComponent } from './layout/common-layout.component';
import { AuthenticationLayoutComponent } from './layout/authentication-layout.component';
import { TemplateService } from './template/template.service';
import { TemplateModule } from './template/template.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpRequestInterceptor } from './core/interceptors/http-request-interceptor';
import { AuthGuard } from './core/auth/auth.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GlobalErrorHandler } from './core/handlers/global-error-handler';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ServerErrorInterceptor } from './core/interceptors/server-error.interceptor';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(AppRoutes, { useHash: true }),
    SharedModule,
    TemplateModule,
    CoreModule,
    MatSnackBarModule,
    BrowserAnimationsModule
  ],
  exports: [

  ],
  declarations: [
    AppComponent,
    CommonLayoutComponent,
    AuthenticationLayoutComponent
  ],
  providers: [
    TemplateService,
    AuthGuard,
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    { provide: HTTP_INTERCEPTORS, useClass: ServerErrorInterceptor, multi: true },
    {provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
    ],
  bootstrap: [AppComponent]
})


export class AppModule { }
