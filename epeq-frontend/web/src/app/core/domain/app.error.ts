export class AppError extends Error{

  erroCode: string | number;
  userMessage: string;
  rootError: any;
  appError: boolean;

  constructor(message: string, error?: any){
    super(message);
    this.rootError = error;
    this.userMessage = "";
    this.appError = true;
  }
}
