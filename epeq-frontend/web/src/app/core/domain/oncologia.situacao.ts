import * as _ from 'lodash';

export class OncologiaSituacao {

  data = [];
  totalSolicitada: number;
  totalAutorizada: number;
  totalDisponibilizado: number;
  totalAgendada: number;

  constructor(){}

  setData(data: any, labelField:string, valueField:string): OncologiaSituacao{
    this.data = [];
    if(data){
      data.forEach(element => {
        this.data.push({
          situacao: element[labelField],
          total: element[valueField]
        })
      });
    }

    this.totalSolicitada = this.getTotalSituacao('SOLICITADO');
    this.totalAutorizada = this.getTotalSituacao('AUTORIZADO');
    this.totalDisponibilizado = this.getTotalSituacao('DISPONIBILIZADO');
    this.totalAgendada = this.getTotalSituacao('AGENDADO');
    return this;
  }

  private getTotalSituacao(nomeSituacao: string): number{
    const situacao = _.find(this.data, { 'situacao': nomeSituacao })
    return situacao ? situacao['total'] : 0;
  }

}
