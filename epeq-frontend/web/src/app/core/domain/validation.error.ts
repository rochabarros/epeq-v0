import { AppError } from "./app.error";

export class ValidationError extends AppError{

  constructor(message: string, error?: any){
    super(message, error);
  }
}
