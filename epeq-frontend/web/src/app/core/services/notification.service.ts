import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ToastService } from '../../shared/toast';
import { ToastType } from '../../shared/toast/toast-config';
import { ErrorUtils } from '../utils/error.utils';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
    public snackBar: MatSnackBar,
    private zone: NgZone,
    private toastService: ToastService) { }

  toastInfo(message: string): void {
    this.showNotification(message, 'info');
  }

  toastError(message: string, error?: object): void {
    const errorMessage = ErrorUtils.selectErrorMessage(message, error);
    this.showNotification(errorMessage, 'warning');
  }

  toastSuccess(message: string): void {
    this.showNotification(message, 'success');
  }

  private showNotification(message: string, typeMessage: ToastType): void {
    this.toastService.show({
      text: message,
      type: typeMessage,
    });
  }

  snackBarSuccess(message: string): void {
    this.zone.run(() => {
      this.snackBar.open(message);
    });
  }

  snackBarError(message: string): void {
    this.zone.run(() => {
      this.snackBar.open(message, 'X', {panelClass: ['snack-bar-error']});
    });
  }
}
