import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { environment } from '../../../environments/environment';
import { Grupo } from '../model/grupo.model';
import { Usuario } from '../model/usuario.model';
import { GrupoDto } from '../dtos/grupo.dto';
import { GRUPOS } from './data/grupos';
import { Observable, throwError, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PermissoesService {

  constructor(private readonly httpService: HttpService) { }

  compareUsuarios(usuario: Usuario, usuario2: Usuario): boolean {
    return usuario.nomeUsuario === usuario2.nomeUsuario;
  }

  listarGrupos(): Observable<Grupo[]> {
    return of(GRUPOS);
  }

  private uriResource(params = ''): string{
    return `${environment.uri_base}/grupos${params}`;
  }

  obterGrupoPorId(id: string): Observable<Grupo> {
    return this.httpService.getById<Grupo>(this.uriResource('/:id'), id);
  }

  criar(grupoDto: GrupoDto): Observable<Grupo> {
    this.validarDados(grupoDto);
    return this.httpService.post<Grupo>(this.uriResource(), grupoDto);
  }

  atualizar(id: string, grupoDto: GrupoDto): Observable<Grupo> {
    this.validarDados(grupoDto);
    return this.httpService.put<Grupo>(this.uriResource('/:id'), id, grupoDto);
  }

  excluir(id: string): Observable<string> {
    return this.httpService.delete(this.uriResource('/:id'), id);
  }

  validarDados(grupoDto: GrupoDto): Observable<Grupo> {
    if (grupoDto.nomeGrupo == null || grupoDto.nomeGrupo == '') {
      return throwError(new Error("O Grupo é obrigatório. "));
    }
  }
}
