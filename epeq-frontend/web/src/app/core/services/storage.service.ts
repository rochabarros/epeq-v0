import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  setCryptValue(key: string, value: string) {
    this.setValue(key, value);
  }

  setCryptObject(key: string, value: any) {
    this.setObject(key, value);
  }

  getCryptValue(key: string){
    return this.getValue(key);
  }

  getCryptObject(key: string){
    return this.getObject(key);
  }

  setValue(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  setObject(key: string, value: any) {
    this.setValue(key, JSON.stringify(value));
  }

  getValue(key: string): string {
    return localStorage.getItem(key);
  }

  getObject(key: string): any {
    let value = this.getValue(key);
    return JSON.parse(value);
  }

  removeItem(key: string){
    localStorage.removeItem(key);
  }

  removeCryptItem(key: string){
    this.removeItem(key);
  }

  clear(){
    localStorage.clear();
  }

}
