import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { HttpService } from './http.service';
import { BehaviorSubject, Observable, from, throwError, of } from 'rxjs';
import { filter, switchMap, map, catchError, tap, take } from 'rxjs/operators';
import { Usuario } from '../model/usuario.model';

import { USUARIOS } from './data/usuarios';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  TOKEN: string = "_token";
  USUARIO: string = "_user";

  constructor(
    private storageService: StorageService,
    private httpService: HttpService
  ) { }

  isLoginSubject = new BehaviorSubject<boolean>(this.hasToken());

  isLoggedIn(): Observable<boolean> {
    return this.isLoginSubject.asObservable();
  }


  login(username: string, password: string): Observable<Usuario> {
    const logins = USUARIOS.filter(
      usuario => usuario.codUsuario.toUpperCase() === username.toUpperCase());
    if (logins && logins.length === 1) {
      return of(logins[0]);
    } else {
      return throwError(Error('Usuário ou Senha inválidos'));
    }
  }

  logout(): void {
    this.invalidadeSession()
  }

  invalidadeSession(): void {
    this.storageService.clear();
    this.isLoginSubject.next(false);
  }

  startSession(usuario: Usuario): void {
    this.storageService.setCryptObject(this.USUARIO, usuario);
    this.storageService.setCryptValue(this.TOKEN, usuario.nomeUsuario);
    this.isLoginSubject.next(true);
  }

  private hasToken(): boolean {
    return !!this.storageService.getCryptValue(this.TOKEN);
  }
}
