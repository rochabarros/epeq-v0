import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  private buildUriPathParams(uri: string, pathParams?: any): string{
    let newUri = uri;
    for (let k in pathParams) {
      newUri = newUri.replace(`:${k}`, pathParams[k]);
    }
    return newUri;
  }

  private buildHttpParams(params?: any, reqOpts?: any): any {

    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }

    return reqOpts;
  }

  get(uri: string, pathParams?: any, params?: any, reqOpts?: any): Observable<any> {
    uri = this.buildUriPathParams(uri, pathParams);
    reqOpts = this.buildHttpParams(params, reqOpts);
    return this.http.get(uri, reqOpts);
  }

  getById<T>(uri: string, id: string): Observable<T> {
    return this.http.get<T>(this.buildUriPathParams(uri, {id: id}));
  }

  post<T>(uri: string, data: any): Observable<T> {
    return this.http.post<T>(uri, data);
  }

  put<T>(uri: string, id: string, data: any): Observable<T> {
    return this.http.put<T>(this.buildUriPathParams(uri, {id: id}), data);
  }

  delete<T>(uri: string, id: string) {
    return this.http.delete<T>(this.buildUriPathParams(uri, {id: id}));
  }
}
