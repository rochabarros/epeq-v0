import { Injectable } from '@angular/core';
import { Usuario } from '../model/usuario.model';
import { Grupo } from '../model/grupo.model';
import { HttpService } from './http.service';
import { environment } from '../../../environments/environment';
import { UsuarioDto } from '../dtos/usuario.dto';
import { StorageService } from './storage.service';
import { USUARIOS } from './data/usuarios';
import { Observable, throwError, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(
    private httpService: HttpService,
    private storageServices: StorageService) { }

  private uriResource(params = ''): string {
    return `${environment.uri_base}/usuarios${params}`;
  }

  getUsuarioLogado(): Usuario {
    return this.storageServices.getCryptObject("_user");
  }

  compareGrupos(grupo: Grupo, grupo2: Grupo): boolean {
    return grupo.nomeGrupo === grupo2.nomeGrupo;
  }

  listar(): Observable<Usuario[]> {
    return of(USUARIOS);
  }

  obterPorId(id: string): Observable<Usuario> {
    return this.httpService.getById<Usuario>(this.uriResource('/:id'), id);
  }

  validarDadosUsuario(usuarioDto: UsuarioDto): Observable<Usuario> {
    if (!usuarioDto.grupos || usuarioDto.grupos.length == 0) {
      return throwError(new Error("É necessário adicionar pelo menos 1 grupo."));
    }
  }

  criarUsuario(usuarioDto: UsuarioDto): Observable<Usuario> {
    this.validarDadosUsuario(usuarioDto);
    return this.httpService.post<Usuario>(this.uriResource(), usuarioDto);
  }

  atualizarUsuario(id: string, usuarioDto: UsuarioDto): Observable<Usuario> {
    this.validarDadosUsuario(usuarioDto);
    return this.httpService.put<Usuario>(this.uriResource('/:id'), id, usuarioDto);
  }

  excluirUsuario(id: string): Observable<string> {
    return this.httpService.delete(this.uriResource('/:id'), id);
  }
}
