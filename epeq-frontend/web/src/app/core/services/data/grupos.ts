import { USUARIOS } from './usuarios';

export const GRUPOS = [
  {
    "codGrupo": "1",
    "nomeGrupo": "ADM",
    "usuarios": [USUARIOS[0]]
  },
  {
    "codGrupo": "2",
    "nomeGrupo": "FARMACIA",
    "usuarios": [USUARIOS[1]]
  },
  {
    "codGrupo": "3",
    "nomeGrupo": "ENFERMAGEM",
    "usuarios": [USUARIOS[2]]
  }
]
