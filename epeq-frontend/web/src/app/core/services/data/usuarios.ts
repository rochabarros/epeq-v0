import { Usuario } from "../../model/usuario.model"
import { Grupo } from "../../model/grupo.model"

const grupoFarmacia = new Grupo();
grupoFarmacia.codGrupo = "ADMIN";

const grupoAdmin = new Grupo();
grupoAdmin.codGrupo = "ADMIN";

const grupoEnfermagem = new Grupo();
grupoEnfermagem.codGrupo = "ENFERMAGEM";

const usuario = new Usuario();
usuario.codUsuario = "ARTHURBARROS";
usuario.nomeUsuario = "Arthur Rocha Barros";
usuario.ativo = true;
usuario.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InJvY2hhLmJhcnJvc0BnbWFpbC5jb20iLCJzdWIiOiI1ZDkyY2ZkMmNlMjgxZjMwNDc0OTA4ZWQiLCJpYXQiOjE1Njk5MDI1NjEsImV4cCI6MTU2OTkwMjYyMX0.vGT299VCdK6mcWpl-EgETkK7lZQefbq7r6o1l4PHu7Y";
usuario.grupos = [grupoAdmin];

const usuario2 = new Usuario();
usuario2.codUsuario = "MATEUSBASTOS";
usuario2.nomeUsuario = "Mateus Bastos Cavalcante";
usuario2.ativo = true;
usuario2.token = "eaJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InJvY2hhLmJhcnJvc0BnbWFpbC5jb20iLCJzdWIiOiI1ZDkyY2ZkMmNlMjgxZjMwNDc0OTA4ZWQiLCJpYXQiOjE1Njk5MDI1NjEsImV4cCI6MTU2OTkwMjYyMX0.vGT299VCdK6mcWpl-EgETkK7lZQefbq7r6o1l4PHu7Y";
usuario2.grupos = [grupoFarmacia];

const usuario3 = new Usuario();
usuario3.codUsuario = "JOAOBARROS";
usuario3.nomeUsuario = "João Barros";
usuario3.ativo = true;
usuario3.token = "ebJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InJvY2hhLmJhcnJvc0BnbWFpbC5jb20iLCJzdWIiOiI1ZDkyY2ZkMmNlMjgxZjMwNDc0OTA4ZWQiLCJpYXQiOjE1Njk5MDI1NjEsImV4cCI6MTU2OTkwMjYyMX0.vGT299VCdK6mcWpl-EgETkK7lZQefbq7r6o1l4PHu7Y";
usuario3.grupos = [grupoEnfermagem];

export const USUARIOS = [usuario, usuario2, usuario3];