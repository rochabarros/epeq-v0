import { Historico } from "../../model/historico.model"

const historico = new Historico();
historico.dataHistorico = new Date();
historico.codUsuario = "ARTHURBARROS";
historico.situacao = "SOLICITADO";

export const AUTORIZACOES = [
  {
    "codBeneficiario": "69574118428",
    "nomeBeneficiario": "Ângelo Victor Steele",
    "nomeMedicamento": "L-Asparaginase",
    "numSolicitacao": "45632760146",
    "numAutorizacao": "0",
    "situacao": "SOLICITADO",
    "dataAgendamento": new Date(),
    "dataAutorizacao": new Date(),
    "dataSolicitacao": new Date(),
    "historico": [historico]
  }
]
