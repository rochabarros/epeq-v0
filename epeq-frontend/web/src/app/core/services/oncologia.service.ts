import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable, of } from 'rxjs';
import { FiltrosPeriodos } from '../dtos/filtros-periodos.dto';
import { Env } from '../../shared/config/enviroment';
import { Oncologia } from '../model/oncologia.model';

@Injectable({
  providedIn: 'root'
})
export class OncologiaService {

  constructor(private readonly httpService: HttpService) { }

  agruparQuantidadeMedicamentos(filtrosPeriodos: FiltrosPeriodos): Observable<any[]>{
    return this.httpService.post(Env.uri('/oncologia/kpis/medicamentos/qtde'), filtrosPeriodos);
  }

  getTotalSituacoes(filtrosPeriodos: FiltrosPeriodos): Observable<any[]>{
    return this.httpService.post(Env.uri('/oncologia/kpis/situacoes/total'), filtrosPeriodos);
  }

  disponibilizarMedicamentos(medicamento: string, dataDisponibilidade: Date, solicitacoes: Oncologia[]): Observable<any[]>{
    return this.httpService.post(Env.uri('/oncologia/disponibilizarMedicamento'), {
      medicamento: medicamento,
      solicitacoes: solicitacoes,
      usuario: 'ARTHURBARROS',
      dataDisponibilidade: dataDisponibilidade
    });
  }
}
