import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StorageService } from './storage.service';
import { HttpService } from './http.service';
import { OncologiaService } from './oncologia.service';
import { PermissoesService } from './permissoes.service';
import { AuthService } from './auth.service';
import { NotificationService } from './notification.service';

@NgModule({
  declarations: [],
  exports: [
  ],
  imports: [
    CommonModule
  ],
  providers:[
    HttpService,
    AuthService,
    StorageService,
    OncologiaService,
    PermissoesService,
    NotificationService
  ]
})
export class ServicesModule { }
