export class ErrorUtils{

  static isUserError(error: object): boolean{
    return error['appError'];
  }

  static selectErrorMessage(message:string, error?: object): any{
    let errorMessage: string;
    if (error) {
      if (ErrorUtils.isUserError(error)) {
        errorMessage = error['message'];
      } else {
        errorMessage = message;
        console.error(`${message} :  ${error['message']}`);
      }
    }else{
      errorMessage = message;
    }
    return errorMessage;
  }
}
