import * as moment from 'moment';
import { Periodos } from "../domain/periodos";

export class DateUtils {

  private static DATE_TEXT_PATTERN: string = "DD/MM/YYYY";

  static strToPeriodo(periodo: string): Periodos {
    return Periodos[periodo];
  }

  static formatPeriodoToStr(periodo: string, ): string {
    const periodos = DateUtils.strToPeriodo(periodo);
    const datas = DateUtils.extractDatasPeriodo(periodos, "");
    const dataInicialStr = DateUtils.formatDate(datas.dataInicial, DateUtils.DATE_TEXT_PATTERN);
    const dataFinalStr = DateUtils.formatDate(datas.dataFinal, DateUtils.DATE_TEXT_PATTERN);
    return `${dataInicialStr} - ${dataFinalStr}`;
  }

  static now(): Date {
    return DateUtils.momentUTC().toDate();
  }

  static nowToStr(): string {
    return DateUtils.formatDate(DateUtils.now(), DateUtils.DATE_TEXT_PATTERN);
  }

  static getDatasIntervaloPeriodo(periodo: string, intervalo: string): object {
    const periodos = DateUtils.strToPeriodo(periodo);
    return DateUtils.extractDatasPeriodo(periodos, intervalo);
  }

  static momentUTC(): moment.Moment {
    return moment().utc();
  }

  static parseDate(dataStr: string, pattern?: string): Date {
    if (!pattern) {
      pattern = 'DD/MM/YYYY';
    }
    return moment(dataStr, pattern).toDate();
  }

  static formatDate(data: Date, pattern?: string): string {
    if (!pattern) {
      pattern = 'YYYY-MM-DD HH:mm:ss';
    }
    return moment(data).format(pattern);
  }

  static extractDatasPeriodo(periodo: Periodos, intervalo: string): any {
    let dataInicial: Date;
    let dataFinal: Date;
    switch (periodo) {
      case Periodos.HOJE: {
        dataInicial = DateUtils.momentUTC().startOf('day').toDate();
        dataFinal = DateUtils.momentUTC().endOf('day').toDate();
        break;
      }
      case Periodos.SEMANA_ATUAL: {
        dataInicial = DateUtils.momentUTC().startOf('week').toDate();
        dataFinal = DateUtils.momentUTC().endOf('week').toDate();
        break;
      }
      case Periodos.ULTIMOS_SETE_DIAS: {
        dataInicial = DateUtils.momentUTC().subtract(7, 'days').startOf('day').toDate();
        dataFinal = DateUtils.momentUTC().subtract(1, 'days').endOf('day').toDate();
        break;
      }
      case Periodos.ULTIMOS_QUINZE_DIAS: {
        dataInicial = DateUtils.momentUTC().subtract(15, 'days').startOf('day').toDate();
        dataFinal = DateUtils.momentUTC().subtract(1, 'days').endOf('day').toDate();
        break;
      }
      case Periodos.ULTIMOS_TRINTA_DIAS: {
        dataInicial = DateUtils.momentUTC().subtract(30, 'days').startOf('day').toDate();
        dataFinal = DateUtils.momentUTC().subtract(1, 'days').endOf('day').toDate();
        break;
      }
      case Periodos.MES_ATUAL: {
        dataInicial = DateUtils.momentUTC().startOf('month').toDate();
        dataFinal = DateUtils.momentUTC().endOf('month').toDate();
        break;
      }
      case Periodos.INTERVALO: {
        let values = intervalo.split(' - ');
        dataInicial = moment(`${values[0]} 00:00:00`, 'DD/MM/YYYY HH:mm:ss').toDate();
        dataFinal = moment(`${values[1]} 23:59:59`, 'DD/MM/YYYY HH:mm:ss').toDate();
        break;
      }
    }
    return {
      dataInicial: dataInicial,
      dataFinal: dataFinal
    }
  }
}
