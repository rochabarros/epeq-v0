import { Grupo } from "./grupo.model";

export class Usuario{
  codUsuario: string;
  nomeUsuario: string;
  token: string;
  ativo: boolean;
  grupos: Grupo[];
}