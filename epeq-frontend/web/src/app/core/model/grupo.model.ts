import { Usuario } from "./usuario.model";

export class Grupo {
  codGrupo: string;
  nomeGrupo: string;
  usuarios: Usuario[];
}
