import { Historico } from "./historico.model";

export class Oncologia {
  codBeneficiario: string;
  nomeBeneficiario: string;
  nomeMedicamento: string;
  numSolicitacao: string;
  numAutorizacao: string;
  situacao: string;
  dataAgendamento: Date;
  dataSolicitacao: Date;
  dataAutorizacao: Date;
  historico: Array<Historico>;
}