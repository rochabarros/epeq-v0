import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesModule } from './services/services.module';

@NgModule({
  exports: [ServicesModule],
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
