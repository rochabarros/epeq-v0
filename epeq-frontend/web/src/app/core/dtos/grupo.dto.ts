import { Usuario } from '../model/usuario.model';

export class GrupoDto {
  nomeGrupo: string;
  usuarios: Array<Usuario>;

  constructor(nomeGrupo: string, usuarios?: Usuario[]) {
    this.nomeGrupo = nomeGrupo;
    this.usuarios = usuarios;
  }
}