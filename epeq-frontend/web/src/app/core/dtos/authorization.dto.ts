import { Usuario } from "../model/usuario.model";

export class AuthorizationDto {
    accessToken: string;
    user: Usuario;
}