import { Grupo } from '../model/grupo.model';

export class UsuarioDto {
  nome: string;
  grupos: Array<Grupo>;

  constructor(values?: any, grupos?: Grupo[]) {

    if (values) {
      this.nome = values['nome'];
    }
    if (grupos) {
      this.grupos = grupos;
    }
  }
}
