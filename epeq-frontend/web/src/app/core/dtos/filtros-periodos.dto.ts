export class FiltrosPeriodos{
  periodo: string;
  intervalo: string;

  constructor(filtrosPeriodos?: any){
    if(filtrosPeriodos){
      const objPeriodo = filtrosPeriodos['periodo'];
      this.periodo = objPeriodo.tipo;
      this.intervalo = objPeriodo.valor;
    }
  }
}
