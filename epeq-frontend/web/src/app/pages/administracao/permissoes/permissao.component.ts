import { Component, OnInit } from '@angular/core';
import { PermissoesService } from '../../../core/services/permissoes.service';
import { Grupo} from '../../../core/model/grupo.model';
import { Usuario } from '../../../core/model/usuario.model';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { GrupoDto } from '../../../core/dtos/grupo.dto';
import * as _ from 'lodash';

@Component({
  selector: 'app-permissao',
  templateUrl: './permissao.component.html'
})
export class PermissaoComponent implements OnInit {

  grupoForm: FormGroup;
  usuarioForm: FormGroup;
  grupo: Grupo;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private currentRoute: ActivatedRoute,
    private permissoesService: PermissoesService) { }

  ngOnInit() {
    this.createFormGroupGrupo();
    this.createFormGroupUsuario();
    this.loadDataPermissao();
  }

  loadDataPermissao() {
    let id = this.currentRoute.snapshot.paramMap.get('id');
    if (id && id !== '0') {
      this.permissoesService.obterGrupoPorId(id).subscribe(
        data => {
          this.grupo = data;
          this.grupoForm.patchValue(this.grupo);
        },
        error => {
          
        }
      );
    } else {
      this.grupo = new Grupo();
      this.grupo.usuarios = Array<Usuario>();
    }
  }

  addUsuario() {
    this.usuarioForm.reset();
  }

  createFormGroupGrupo() {
    this.grupoForm = this.formBuilder.group({
      nomeGrupo: ['', Validators.required]
    });
  }

  createFormGroupUsuario() {
    this.usuarioForm = this.formBuilder.group({
      nomeUsuario: ['', Validators.required]
    });
  }

  validateForm(): boolean {
    return this.grupoForm.valid;
  }

  isFieldGrupoFormInvalid(fieldName: string) {
    return this.grupoForm.controls[fieldName].invalid && this.grupoForm.controls[fieldName].touched;
  }

  isFieldUsuarioFormInvalid(fieldName: string) {
    return this.usuarioForm.controls[fieldName].invalid && this.usuarioForm.controls[fieldName].touched;
  }

  salvarUsuario() {
    let usuarioFound = false;
    let usuario: Usuario = this.usuarioForm.value;

    this.grupo.usuarios.forEach((item, index, arrUsuarios) => {
      if (this.permissoesService.compareUsuarios(item, usuario)) {
        arrUsuarios[index] = usuario;
        usuarioFound = true;
      }
    })
    if (!usuarioFound) {
      this.grupo.usuarios.push(usuario);
    }
    this.usuarioForm.reset();
  }

  excluirUsuario(usuario: Usuario) {
    this.grupo.usuarios = _.remove(this.grupo.usuarios, (item: Usuario) => {
      return !this.permissoesService.compareUsuarios(item, usuario);
    });
  }

  cancelar() {
    this.router.navigate(['/administracao/permissoes']);
  }

  salvar() {
    if (this.validateForm()) {
      const grupoDto: GrupoDto = new GrupoDto(this.grupoForm.value, this.grupo.usuarios)
      
      if (this.isGrupoNovo()) {
        this.permissoesService.criar(grupoDto).subscribe(
          res => {
            this.onSalvarGrupo(res);
          },
          error => {
            
          }
        )
      } else {
        this.permissoesService.atualizar(this.grupo.codGrupo, grupoDto).subscribe(
          res => {
            this.onSalvarGrupo(res);
          },
          error => {

          }
        )
      }
    } else {
      this.validateAllFormFields(this.grupoForm);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isGrupoNovo(): boolean {
    return this.grupo == undefined || this.grupo.codGrupo == undefined || this.grupo.codGrupo === "0";
  }

  onSalvarGrupo(res: any) {
    this.router.navigate(['/administracao/permissoes']);
  }
}
