import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataTablesHelper } from '../../../shared/datatables/datatables-helper';
import { Grupo } from '../../../core/model/grupo.model';
import { PermissoesService } from '../../../core/services/permissoes.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-permissoes',
  templateUrl: './permissoes.component.html'
})
export class PermissoesComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  grupos: Grupo[] = [];

  constructor(private router: Router,
              private permissoesService: PermissoesService) { }

  ngOnInit() {
    const dataTablesHelper = new DataTablesHelper();
    this.dtOptions = dataTablesHelper.getOptions();
    this.listarGrupos();
  }

  listarGrupos() {
    this.permissoesService.listarGrupos().subscribe(
      data => {
        this.grupos = data;
      }
    )
  }

  openForEdit(id: string) {
    this.router.navigate(['/administracao/permissoes/' + id]);
  }

  novo() {
    this.router.navigate(['/administracao/permissoes/0']);
  }

  onDelete(id: string) {
    swal({
      title: "Exclusão",
      text: "Você deseja realmente excluir essa permissão ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Sim, desejo excluir"
    }).then(
      (result) => {
        if (result.value) {
          this.permissoesService.excluir(id).subscribe(
            data => {
              swal(
                'Excluído',
                'Permissão excluída com sucesso',
                'success'
              )
            },
            error => {

            }
          );
        }
      }
    )
  }
}
