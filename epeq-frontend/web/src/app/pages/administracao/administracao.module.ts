import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PermissoesComponent } from './permissoes/permissoes.component';
import { PermissaoComponent } from './permissoes/permissao.component';
import { DataTablesModule } from 'angular-datatables';
import { ThemeConstants } from '../../shared/config/theme-constant';
import { NvD3Module } from 'ng2-nvd3';
import 'nvd3';
import 'd3';

const AdministracaoRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'permissoes',
        component: PermissoesComponent,
        data: {
          title: 'Permissões'
        }
      },
      {
        path: 'permissoes/:id',
        component: PermissaoComponent,
        data: {
          title: 'Permissão'
        }
      }
    ]
  }
];

@NgModule({
  declarations: [PermissoesComponent, PermissaoComponent],
  imports: [
    RouterModule.forChild(AdministracaoRoutes),
    DataTablesModule,
    NvD3Module,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[
    ThemeConstants
  ]
})
export class AdministracaoModule { }
