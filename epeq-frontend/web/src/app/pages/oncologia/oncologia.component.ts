import { Component, OnInit } from '@angular/core';
import { OncologiaService } from '../../core/services/oncologia.service';
import { ThemeConstants } from '../../shared/config/theme-constant';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FiltrosPeriodos } from '../../core/dtos/filtros-periodos.dto';
import { BarChart } from '../../shared/charts/bar-chart';
import { PieChart } from '../../shared/charts/pie-chart';
import { OncologiaSituacao } from '../../core/domain/oncologia.situacao';
import { Router } from '@angular/router';
import { DataTableStoreFilters } from '../../shared/datatables/datatable-storage';
import { APP } from '../../app.constantes';

@Component({
  selector: 'app-oncologia',
  templateUrl: './oncologia.component.html'
})
export class OncologiaComponent implements OnInit {

  medicamentosbarChart: BarChart;
  situacoesPieChart: PieChart;
  oncologiaSituacoes = new OncologiaSituacao();
  filtroForm: FormGroup;
  showIntervalo = false;

  constructor(
    private router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly oncologiaService: OncologiaService,
    private readonly colorConfig: ThemeConstants) { }

  ngOnInit() {
    this.createFormGroupFiltro();
    this.initMedicamentosBarChar();
    this.initSituacoesPieChart();
    this.loadChartsData();
  }

  initMedicamentosBarChar() {
    this.medicamentosbarChart = new BarChart();
    this.medicamentosbarChart.setColors(this.colorConfig.get().colors)
      .setAxisLabes("MEDICAMENTOS", "QUANTIDADE")
      .setDatasetName("MEDICAMENTOS")
      .init();
  }

  initSituacoesPieChart() {
    this.situacoesPieChart = new PieChart();
    this.situacoesPieChart.setColors(this.colorConfig.get().colors).init();
  }

  createFormGroupFiltro() {
    this.filtroForm = this.formBuilder.group({
      periodo: [{
        tipo:'MES_ATUAL',
        valor: ''
      }, Validators.required],
    });
  }

  filtrar(){
    this.loadChartsData();
  }

  abrirSolicitacoes(situacao: string) {
    if (this.filtroForm.valid) {
      const objPeriodo = this.filtroForm.controls['periodo'].value;
      DataTableStoreFilters.newFilter(APP.FILTROS_GRID_SOLICITACOES)
                            .set('situacao', situacao)
                            .setPeriodo('dataSolicitacao', objPeriodo['tipo'], objPeriodo['valor'])
                            .store();
      this.router.navigate(['/oncologia/solicitacoes']);
    }
  }

  loadChartsData() {
    if (this.filtroForm.valid) {
      const filtrosPeriodos: FiltrosPeriodos = new FiltrosPeriodos(this.filtroForm.value);
      this.findMedicamentosChartData(filtrosPeriodos);
      this.findTotalSituacoes(filtrosPeriodos);
    }
  }

  findMedicamentosChartData(filtrosPeriodos: FiltrosPeriodos) {
    this.oncologiaService
      .agruparQuantidadeMedicamentos(filtrosPeriodos)
      .subscribe(
        (res) => {
          this.medicamentosbarChart.setData(res, "_id", "total").rerender();
        }
      );
  }

  findTotalSituacoes(filtrosPeriodos: FiltrosPeriodos) {
    this.oncologiaService
      .getTotalSituacoes(filtrosPeriodos)
      .subscribe(
        (res) => {
          this.oncologiaSituacoes.setData(res, "_id", "total");
          this.situacoesPieChart.setData(res, "_id", "total").rerender();
        }
      );
  }

}
