import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DataTablesHelper } from '../../../shared/datatables/datatables-helper';
import { Oncologia } from '../../../core/model/oncologia.model';
import { HttpService } from '../../../core/services/http.service';
import { DataTablesResponse } from '../../../shared/datatables/datatables-response';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Env } from '../../../shared/config/enviroment';
import { NotificationService } from '../../../core/services/notification.service';
import { APP } from '../../../app.constantes';
import * as _ from 'lodash';
import { FormHelper } from '../../../shared/forms/form-helper';
import { OncologiaService } from '../../../core/services/oncologia.service';
import { DateUtils } from '../../../core/utils/date-utils';

@Component({
  selector: 'app-solicitacoes',
  templateUrl: './solicitacoes.component.html'
})
export class SolicitacoesComponent implements OnInit, AfterViewInit {

  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  solicitacoes: Oncologia[] = [];
  medicacaoForm: FormGroup;
  medicamentosSelecionados = [];
  solicitacoesSelecionadas = {};
  resultadosDisponibilizacao = [];
  datatablesHelper: DataTablesHelper;
  formHelper: FormHelper;

  control: FormControl;
  // customErrors = {required: 'Please accept the terms'}

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly httpService: HttpService,
    private readonly notificationService: NotificationService,
    private readonly oncologiaService: OncologiaService) { }

  ngOnInit() {
    this.datatablesHelper = new DataTablesHelper()
      .storeFilters(true, APP.FILTROS_GRID_SOLICITACOES)
      .setServerOptions(10, true)
      .setPaginate(true)
      .setSearch(true)
      .startOrder(1, "asc")
      .addColumn("_id", false, false)
      .addColumn("nomeBeneficiario")
      .addColumn("nomeMedicamento")
      .addColumn("dataSolicitacao")
      .addColumn("dataAutorizacao")
      .addColumn("dataDisponibilidade")
      .addColumn("dataAgendamento")
      .addColumn("situacao")
      .setAjax(this.httpService, Env.uri('/oncologia/datatable/pesquisa'),
        (resp: DataTablesResponse) => {
          this.solicitacoes = resp.data;
        }
      );

    this.dtOptions = this.datatablesHelper.getOptions();
    this.createFormGroupMedicacao();
  }

  ngAfterViewInit(): void {
    this.datatablesHelper.configFilters(this.datatableElement);
  }

  limparSelecionados(): void {
    this.datatablesHelper.clearSelectedData();
    this.notificationService.snackBarError('Todos os itens foram deselecionados. Lista de seleção vazia!')
  }

  tratarErroSelecaoItens(error: any): void {
    this.notificationService.snackBarError('Erro ao selecionar itens da lista de solicitações');
  }

  selecionarTodosCheckbox(e): void {
    try {
      this.datatablesHelper.selectItemsByEvent(e, this.solicitacoes);
    } catch (error) {
      this.tratarErroSelecaoItens(error);
    }
  }

  selecionarCheckbox(e, data: any): void {
    try {
      this.datatablesHelper.selectItemByEvent(e, data);
    } catch (error) {
      this.tratarErroSelecaoItens(error);
    }
  }

  filtrarLista(): void {
    this.datatablesHelper.applyFilters(this.datatableElement);
  }

  limparFiltrosLista(): void {
    this.datatablesHelper.applyClearFiltersForm();
  }

  createFormGroupMedicacao(): void {
    this.control = this.formBuilder.control('', Validators.required);
    this.medicacaoForm = this.formBuilder.group({
      nomeMedicamento: ['', Validators.required],
      disponibilidade: ['N'],
      dataDisponibilidade: ['', Validators.required]
    });
    this.formHelper = new FormHelper(this.medicacaoForm);
  }

  opcaoDisponibilidade(e) {
    if (e.target.value === 'S') {
      this.formHelper.value('dataDisponibilidade', DateUtils.nowToStr());
    } else if (e.target.value === 'N') {
      this.formHelper.value('dataDisponibilidade', "");
    }
  }

  onSubmit() {
    if (this.formHelper.isValid()) {
      this.salvarDisponilidadeMedicamento();
    }
  }

  openFormDisponibilidade(): void {
    this.setMedicamentosSelecionados();
    this.resultadosDisponibilizacao = [];
  }

  initMedicacaoForm(): void {
    this.formHelper.patchValue('nomeMedicamento', "");
    this.formHelper.patchValue('disponibilidade', "N");
    this.formHelper.patchValue('dataDisponibilidade', "");
  }

  setMedicamentosSelecionados(): void {
    this.solicitacoesSelecionadas = this.datatablesHelper.groupSelectedData("nomeMedicamento");
    if (this.solicitacoesSelecionadas) {
      this.medicamentosSelecionados = _.keys(this.solicitacoesSelecionadas);
    }
  }

  salvarDisponilidadeMedicamento(): void {
    const medicamento = this.formHelper.value('nomeMedicamento');
    const solicitacoes = this.solicitacoesSelecionadas[medicamento];
    if (solicitacoes) {
      const dataDisponibilidade = this.formHelper.dateValue('dataDisponibilidade');
      this.oncologiaService.disponibilizarMedicamentos(medicamento, dataDisponibilidade, solicitacoes)
        .subscribe(resultados => {
          this.resultadosDisponibilizacao.push(resultados);
          this.processarResultadoDisponibilidade(resultados);
        }
        )
    }
  }

  processarResultadoDisponibilidade(resultados: object): void {
    if (resultados['totalErros'] === 0) {
      this.removerOpcaoMedicamento(resultados);
      this.removerSolicitacoesSelecionadas(resultados);
      this.initMedicacaoForm();
      this.filtrarLista();
    }
  }

  removerOpcaoMedicamento(resultados: object): void {
    _.remove(this.medicamentosSelecionados, (medicamento) => {
      return medicamento === resultados['id'];
    });
  }

  removerSolicitacoesSelecionadas(resultados: object): void {
    _.remove(this.datatablesHelper.getSelectedData(), (solicitacao) => {
      this.datatablesHelper.checkedItem(false, solicitacao);
      return _.find(resultados['resultadosOk'], { _id: solicitacao['_id'] });
    });
  }


}
