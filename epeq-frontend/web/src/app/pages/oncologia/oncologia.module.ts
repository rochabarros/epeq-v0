import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SolicitacoesComponent } from './solicitacoes/solicitacoes.component';
import { Routes, RouterModule } from '@angular/router';
import { FluxosComponent } from './fluxos/fluxos.component';
import { DataTablesModule } from 'angular-datatables';
import { OncologiaComponent } from './oncologia.component';
import { ThemeConstants } from '../../shared/config/theme-constant';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { NvD3Module } from 'ng2-nvd3';
import { OncologiaService } from '../../core/services/oncologia.service';
import { SharedModule } from '../../shared/shared.module';
import { AuthGuard } from '../../core/auth/auth.guard';
import 'nvd3';
import 'd3';

const OncologiaRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: OncologiaComponent,
        data: {
          title: 'Resumo'
        }
      },
      {
        path: 'solicitacoes',
        component: SolicitacoesComponent,
        data: {
          title: 'Solicitações'
        }
      },
      {
        path: 'fluxos',
        component: FluxosComponent,
        data: {
          title: 'Fluxos'
        }
      }
    ]
  }
];

@NgModule({
  declarations: [SolicitacoesComponent, FluxosComponent, OncologiaComponent],
  imports: [
    RouterModule.forChild(OncologiaRoutes),
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    NvD3Module,
    NgxMaskModule.forRoot()
  ],
  providers:[
    ThemeConstants,
    OncologiaService
  ]
})
export class OncologiaModule { }
