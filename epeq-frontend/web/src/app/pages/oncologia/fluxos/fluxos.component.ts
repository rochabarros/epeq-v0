import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DataTablesHelper } from '../../../shared/datatables/datatables-helper';
import { Oncologia } from '../../../core/model/oncologia.model';
import { HttpService } from '../../../core/services/http.service';
import { DataTablesResponse } from '../../../shared/datatables/datatables-response';
import { DataTableDirective } from 'angular-datatables';
import { Env } from '../../../shared/config/enviroment';

@Component({
  selector: 'app-fluxos',
  templateUrl: './fluxos.component.html'
})
export class FluxosComponent implements OnInit, AfterViewInit {

  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  solicitacoes: Oncologia[] = [];

  datatablesHelper: DataTablesHelper;

  constructor(private readonly httpService: HttpService) { }

  ngOnInit() {
    this.datatablesHelper = new DataTablesHelper()
      .setServerOptions(10, true)
      .setPaginate(true)
      .setSearch(true)
      .startOrder(0, "asc")
      .addColumn("nomeBeneficiario")
      .addColumn("dataSolicitacao")
      .addColumn("dataAutorizacao")
      .addColumn("dataDisponibilidade")
      .addColumn("dataAgendamento")
      .setAjax(this.httpService, Env.uri('/oncologia/datatable/fluxo'),
        (resp: DataTablesResponse) => {
          this.solicitacoes = resp.data;
        }
      );
    this.dtOptions = this.datatablesHelper.getOptions();
  }

  ngAfterViewInit(): void {
    this.datatablesHelper.configFilters(this.datatableElement);
  }

  filtrarLista() {
    this.datatablesHelper.applyFilters(this.datatableElement);
  }

  limparFiltrosLista(): void {
    this.datatablesHelper.applyClearFiltersForm();
  }
}
