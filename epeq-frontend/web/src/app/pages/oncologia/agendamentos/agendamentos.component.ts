import { Component, OnInit } from '@angular/core';
import { DataTablesHelper } from '../../../shared/datatables/datatables-helper';

@Component({
  selector: 'app-agendamentos',
  templateUrl: './agendamentos.component.html'
})
export class AgendamentosComponent implements OnInit {

  dtOptions: DataTables.Settings = {};

  constructor() { }

  ngOnInit() {
    const dataTablesHelper = new DataTablesHelper();
    this.dtOptions = dataTablesHelper.getOptions();
  }

}
