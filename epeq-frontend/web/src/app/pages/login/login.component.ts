import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  control: FormControl;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private notificationService: NotificationService,
    private authService: AuthService) {
  }

  ngOnInit() {
    this.createFormGroupLogin();
  }

  createFormGroupLogin() {
    this.control = this.formBuilder.control('', Validators.required);

    this.loginForm = this.formBuilder.group({
      usuario: ['', Validators.required],
      senha: ['', Validators.required]
    });
  }

  onSubmit(){
    this.login();
  }

  login() {

    if (this.loginForm.valid) {

      const username = this.loginForm.get('usuario').value;
      const password = this.loginForm.get('senha').value;
      this.authService.login(username, password).subscribe(
        data => {
          this.authService.startSession(data);
          this.router.navigate(['/oncologia']);
        },
        error => {
          this.notificationService.snackBarError(error.message);
        }
      );
    }else{
      this.notificationService.snackBarError("Formulário está inválido para envio");
    }
  }
}
