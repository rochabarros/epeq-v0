import { Component } from '@angular/core';
import { TemplateService } from '../template.service';
import { Usuario } from '../../core/model/usuario.model';
import { UsuariosService } from '../../core/services/usuarios.service';
import { AuthService } from '../../core/services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent  {

    isCollapse: boolean;
    isOpen: boolean;
    usuarioLogado: Usuario;

    constructor(private tplSvc: TemplateService,
        private usuarioService: UsuariosService,
        private router: Router,
        private authService: AuthService) {
    }

    ngOnInit(): void {
        this.usuarioLogado = this.usuarioService.getUsuarioLogado();
        this.tplSvc.isSideNavCollapseChanges.subscribe(isCollapse => this.isCollapse = isCollapse);
        this.tplSvc.isSidePanelOpenChanges.subscribe(isOpen => this.isOpen = isOpen);
    }    
    logout(){
        this.authService.logout();
        this.router.navigate(['/login']);
    }

    toggleSideNavCollapse() {
        this.isCollapse = !this.isCollapse;
        this.tplSvc.toggleSideNavCollapse(this.isCollapse);
    }

    toggleSidePanelOpen() {
        this.isOpen = !this.isOpen;
        this.tplSvc.toggleSidePanelOpen(this.isOpen);
    }
}
