
import { InjectionToken } from '@angular/core';

export const defaultErrors = {
  required: (error) => `O Campo é obrigatório.`,
  minlength: ({ requiredLength, actualLength }) => `Esperado ${requiredLength}, mas o valor está com ${actualLength}`
}

export const FORM_ERRORS = new InjectionToken('FORM_ERRORS', {
  providedIn: 'root',
  factory: () => defaultErrors
});


