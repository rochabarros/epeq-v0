import { FormGroup } from "@angular/forms";
import { DateUtils } from "../../core/utils/date-utils";

export class FormHelper{

  form: FormGroup;

  constructor(form: FormGroup){
    this.form = form;
  }

  isValid(): boolean{
    return this.form.valid;
  }

  values(): any{
    return this.form.value;
  }

  value(formField: string, value?: any): any{
    if(value !== undefined){
      this.form.controls[formField].setValue(value);
    }
    return this.form.controls[formField].value;
  }

  patchValue(formField: string, value?: any): any{
    if(value !== undefined){
      this.form.controls[formField].patchValue(value);
    }
    return this.form.controls[formField].value;
  }

  dateValue(formField: string): any{
    const dateStrValue = this.value(formField);
    if(dateStrValue){
      return DateUtils.parseDate(dateStrValue);
    }
    return null;
  }
}
