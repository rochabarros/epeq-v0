export class PieChart {

  pieChartOption;
  pieChartData;
  settings = {};
  data = [];
  total = 0;

  constructor() { }

  setColors(colors: any): PieChart {
    this.settings['colors'] = colors;
    return this;
  }

  setData(data: any, labelField: string, valueField: string): PieChart {
    this.data = [];
    if (data) {
      this.calculateTotal(data, valueField);
      this.data = [];
      data.forEach(d => {
        this.data.push({
          key: d[labelField],
          y: this.calculaPercentual(d[valueField])
        })
      })
    }
    return this;
  }

  calculaPercentual(valor: number): number{
    return (valor/this.total) * 100
  }

  calculateTotal(data: any, valueField: string){
    data.forEach(d =>{
      this.total += d[valueField]
    })
  }

  init() {
    this.buildChartOption();
  }

  rerender() {
    this.buildChartData();
  }

  private buildChartOption() {
    let themeColors = this.settings['colors'];
    this.pieChartOption = {
      chart: {
        type: 'pieChart',
        height: 350,
        x: function (d) { return d.key; },
        y: function (d) { return d.y; },
        showLabels: true,
        color: [
          themeColors.warning,
          themeColors.info,
          themeColors.success
        ],
        duration: 500,
        labelThreshold: 0.01,
        legend: {
          margin: {
            top: 5,
            right: 35,
            bottom: 5,
            left: 0
          }
        }
      }
    };
  }

  private buildChartData() {
    this.pieChartData = this.data;
  }
}
