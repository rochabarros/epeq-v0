export class BarChart {

  barChartOption;
  barChartData;
  settings = {};
  data = [];

  constructor() { }

  setColors(colors: any): BarChart{
    this.settings['colors'] = colors;
    return this;
  }

  setAxisLabes(xAxisLabel: string, yAxisLabel: string): BarChart{
    this.settings['xAxisLabel'] = xAxisLabel;
    this.settings['yAxisLabel'] = yAxisLabel;
    return this;
  }

  setDatasetName(dataSetName: string,): BarChart{
    this.settings['dataSetName'] = dataSetName;
    return this;
  }

  setData(data: any, labelField: string, valueField: string): BarChart{
    this.data = [];
    if(data){
      data.forEach(d => {
        this.data.push({
          label: d[labelField],
          value: d[valueField]
        })
      })
    }
    return this;
  }

  init(){
    this.buildChartOption();
  }

  rerender(){
    this.buildChartData();
  }

  private buildChartOption(): void {
    let themeColors = this.settings['colors'];
    this.barChartOption = {
      chart: {
        type: 'discreteBarChart',
        height: 350,
        margin: {
          top: 20,
          right: 20,
          bottom: 50,
          left: 55
        },
        x: function (d) { return d.label; },
        y: function (d) { return d.value + (1e-10); },
        color: [
          themeColors.info,
          themeColors.success
        ],
        showValues: true,
        duration: 500,
        xAxis: {
          axisLabel: this.settings['xAxisLabel']
        },
        yAxis: {
          axisLabel: this.settings['yAxisLabel'],
          axisLabelDistance: -10
        }
      }
    };
  }

  private buildChartData(){
    this.barChartData = [
      {
        key: this.settings['dataSetName'],
        values: this.data
      }
    ]
  }
}
