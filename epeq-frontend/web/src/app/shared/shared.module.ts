import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { RouterModule } from "@angular/router";
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpRequestInterceptor } from '../core/interceptors/http-request-interceptor';
import { YesOrNoPipe } from './pipes/yes-or-no-pipe';
import { NgxMaskModule } from 'ngx-mask';
import { CardCounterComponent } from './card-counter/card-counter.component';
import { ToastModule } from './toast';
import { DatatableSelectedRowsPipe } from './datatables/datatable-selected-rows-pipe';
import { ImageGreenOrRedPipe } from './pipes/image-green-or-red-pipe';
import { SelectPeriodosComponent } from './select-periodos/select-periodos.component';
import { ControlErrorsDirective } from './forms/directives/control-errors.directive';
import { ControlErrorComponent } from './forms/components/control-error/control-error.component';
import { ControlErrorContainerDirective } from './forms/directives/control-error-container.directive';
import { FormSubmitDirective } from './forms/directives/form-submit.directive';

@NgModule({
  exports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    PerfectScrollbarModule,
    DataTablesModule,
    NgbModule,
    YesOrNoPipe,
    ImageGreenOrRedPipe,
    DatatableSelectedRowsPipe,
    CardCounterComponent,
    SelectPeriodosComponent,
    NgxMaskModule,
    ControlErrorsDirective,
    ControlErrorComponent,
    ControlErrorContainerDirective,
    FormSubmitDirective
  ],
  imports: [
    RouterModule,
    CommonModule,
    DataTablesModule,
    PerfectScrollbarModule,
    NgbModule,
    ToastModule.forRoot(),
    NgxMaskModule.forRoot()
  ],
  declarations: [
    YesOrNoPipe,
    ImageGreenOrRedPipe,
    DatatableSelectedRowsPipe,
    CardCounterComponent,
    SelectPeriodosComponent,
    ControlErrorsDirective,
    ControlErrorComponent,
    ControlErrorContainerDirective,
    FormSubmitDirective
  ],
  entryComponents: [
    ControlErrorComponent
  ],
  providers: [
    HttpRequestInterceptor
  ]
})

export class SharedModule { }
