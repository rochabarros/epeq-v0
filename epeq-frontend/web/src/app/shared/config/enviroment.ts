import { environment } from "../../../environments/environment";

export class Env{

  static uri(params = ''): string{
    return `${environment.uri_base}${params}`;
  }
}
