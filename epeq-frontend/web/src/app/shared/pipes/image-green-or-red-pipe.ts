import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imageGreenOrRed'
})
export class ImageGreenOrRedPipe implements PipeTransform {
  transform(value: Date): string {
    const greenImage = this.isImageGreen(value);
    return `assets/images/${greenImage ? 'green' : 'red'}.png`;
  }

  isImageGreen(value: Date): boolean {
    return value != null;
  }
}
