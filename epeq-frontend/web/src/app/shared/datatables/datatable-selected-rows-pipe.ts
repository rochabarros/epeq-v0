import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'dtSelectedRows'
})
export class DatatableSelectedRowsPipe implements PipeTransform {

  transform(dataRow: any, targetInput: string, selectedData: any[]): string {
    const isSelected = this.isSelected(dataRow, selectedData);
    return this.formatOutputByTargetInput(isSelected, targetInput);
  }

  isSelected(dataRow: any, selectedData: any[]): boolean {
    if (dataRow && selectedData) {
      const dataFound = _.find(selectedData, { _id: dataRow['_id'] });
      return dataFound === undefined ? false : true;
    }
    return false;
  }

  formatOutputByTargetInput(isSelected: boolean, targetInput: string): string {
    if (targetInput === 'checkbox') {
      return isSelected ? "checked" : "";
    }
    return isSelected ? "true" : "false";
  }
}
