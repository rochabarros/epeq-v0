import { HttpService } from "../../core/services/http.service";
import { DataTablesResponse } from "./datatables-response";
import { DataTableDirective } from "angular-datatables";
import * as _ from 'lodash';
import { DataTableStorage } from "./datatable-storage";

export class DataTablesHelper {

  private options: {};

  private selectedData = [];

  private permissionSelectedFunction: Function;

  private userOptions: {};

  private loadFilters = false;

  constructor() {
    this.options = {
      language: {
        emptyTable: "Nenhum registro encontrado",
        info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        infoEmpty: "Mostrando 0 até 0 de 0 registros",
        infoFiltered: "(Filtrados de _MAX_ registros)",
        infoPostFix: "",
        thousands: ".",
        lengthMenu: "_MENU_ resultados por página",
        loadingRecords: "Carregando...",
        processing: "Processando...",
        zeroRecords: "Nenhum registro encontrado",
        search: "Pesquisar",
        paginate: {
          next: "Próximo",
          previous: "Anterior",
          first: "Primeiro",
          last: "Último"
        },
        aria: {
          sortAscending: ": Ordenar colunas de forma ascendente",
          sortDescending: ": Ordenar colunas de forma descendente"
        }
      }
    }

    this.userOptions = {};
  }

  getOptions(): object {
    return this.options;
  }

  getUserOptions(): object {
    return this.userOptions;
  }

  tableName(tableName: string): DataTablesHelper {
    this.userOptions['tableName'] = tableName;
    return this;
  }

  getTableName(): string {
    return this.userOptions['tableName'];
  }

  storeFilters(storeFilters: boolean, storedFiltersName?: string): DataTablesHelper {
    this.userOptions['storeFilters'] = storeFilters;
    if (storeFilters && storedFiltersName) {
      this.userOptions['storedFiltersName'] = storedFiltersName;
    }
    return this;
  }

  hasStoreFilters(): boolean {
    return this.userOptions['storeFilters'];
  }

  getStoredFilters(): object {
    const storeKeyName = this.userOptions['storedFiltersName'];
    return DataTableStorage.getObject(storeKeyName);
  }

  removeStoredFilters(): DataTablesHelper {
    const storeKeyName = this.userOptions['storedFiltersName'];
    DataTableStorage.removeItem(storeKeyName);
    return this;
  }

  startOrder(columnIndex: number, typeOrder: string): DataTablesHelper {
    if (!this.options['order']) {
      this.options['order'] = [];
    }

    this.options['order'].push([columnIndex, typeOrder]);

    return this;
  }

  addColumnDefs(): DataTablesHelper {
    if (!this.options['columnDefs']) {
      this.options['columnDefs'] = [];
    }
    return this;
  }

  addColumn(data, orderable = true, searchable = true): DataTablesHelper {
    if (!this.options['columns']) {
      this.options['columns'] = [];
    }

    this.options['columns'].push({
      data: data,
      orderable: orderable,
      sortable: orderable,
      searchable: searchable
    });
    return this;
  }

  getColumnsFilters(): any[] {
    return _.filter(this.options['columns'], 'searchable');
  }

  getColumnFilter(columnName): any[] {
    return _.filter(this.options['columns'], {'searchable': true, 'data': columnName});
  }

  setPaginate(paginate: boolean): DataTablesHelper {
    this.options['paginate'] = paginate;
    if (paginate) {
      this.options['processing'] = true;
      this.options['pagingType'] = 'full_numbers';
    }
    return this;
  }

  setData(data: any): DataTablesHelper {
    this.options['data'] = data;
    return this;
  }

  setOrder(order: boolean): DataTablesHelper {
    this.options['order'] = order;
    return this;
  }

  setSearch(search: boolean): DataTablesHelper {
    this.options['searching'] = search;
    return this;
  }

  setServerOptions(pageLength: number, serverSide: boolean): DataTablesHelper {
    this.options['pageLength'] = pageLength;
    this.options['serverSide'] = serverSide;
    return this;
  }

  setColumnsSearchValues(dataTablesParameters: any): DataTablesHelper {
    const columns = dataTablesParameters.columns;
    columns.forEach(column => {
      if (column['searchable']) {
        column['search']['value'] = this.getFormFilterValue(column['data'])
      }
    });
    return this;
  }

  setAjax<T>(httpService: HttpService, uri: string, fn: any): DataTablesHelper {
    this.options['ajax'] = (dataTablesParameters: any, callback) => {
      httpService
        .post<DataTablesResponse>(
          uri,
          dataTablesParameters
        ).subscribe(resp => {
          fn(resp);

          callback({
            recordsTotal: resp.recordsTotal,
            recordsFiltered: resp.recordsFiltered,
            data: []
          });
        });
    }
    return this;
  }

  getSelectedData(): any[] {
    return this.selectedData;
  }

  clearSelectedData(): DataTablesHelper {
    this.selectedData = [];
    this.checkedItems(false);
    return this;
  }

  getTotalSelectedData(): number {
    return this.selectedData.length;
  }

  getFirstSelectedData(): any {
    return this.hasSelectedData() ? this.selectedData[0] : null;
  }

  addSelectedData(data: any): DataTablesHelper {
    const dataFound = _.find(this.getSelectedData(), { _id: data['_id'] });
    if (!dataFound) {
      this.selectedData.push(data);
    }
    return this;
  }

  removeSelectedData(data: any): DataTablesHelper {
    _.remove(this.selectedData, (element) => {
      return data._id === element._id;
    });
    return this;
  }

  hasSelectedData(): boolean {
    return this.selectedData.length > 0;
  }

  addSelectedDatas(selectedDatas: any[]): DataTablesHelper {
    if (selectedDatas) {
      selectedDatas.forEach(data => {
        this.addSelectedData(data);
      })
    }
    return this;
  }

  removeSelectedDatas(selectedDatas: any[]): DataTablesHelper {
    if (selectedDatas) {
      selectedDatas.forEach(data => {
        this.removeSelectedData(data);
      })
    }
    return this;
  }

  setPermissionSelectedFunction(fn: Function): DataTablesHelper {
    this.permissionSelectedFunction = fn;
    return this;
  }

  hasPermissionToSelectedItem(): boolean {
    return this.permissionSelectedFunction !== undefined;
  }

  addDataWithoutPermission(selecionado: boolean, data: any): DataTablesHelper {
    if (selecionado) {
      this.addSelectedData(data);
    } else {
      this.removeSelectedData(data);
    }
    return this;
  }

  addDataWithPermission(selecionado: boolean, data: any): DataTablesHelper {
    if (selecionado) {
      if (this.permissionSelectedFunction(this, data)) {
        this.addSelectedData(data);
      }
    } else {
      this.removeSelectedData(data);
    }
    return this;
  }

  selectItemsByEvent(event, datas: any): void {
    if (datas) {
      datas.forEach(data => {
        try {
          this.selectItem(event.target.checked, data);
          this.checkedItem(event.target.checked, data);
        } catch (error) {
          event.target.checked = false;
          this.checkedItem(event.target.checked, data);
          throw error;
        }
      })
    }
  }

  selectItemByEvent(event, data: any): void {
    try {
      this.selectItem(event.target.checked, data);
    } catch (error) {
      event.target.checked = false;
      throw error;
    }
  }

  selectItem(selecionado: boolean, data: any): void {
    if (this.hasPermissionToSelectedItem()) {
      this.addDataWithPermission(selecionado, data);
    } else {
      this.addDataWithoutPermission(selecionado, data);
    }
  }

  checkedItem(checked: boolean, data: any): DataTablesHelper {
    $(`input[name='${data['_id']}']`).prop('checked', checked);
    return this;
  }

  checkedItemSelectAll(checked: boolean): DataTablesHelper {
    $(`input[name='']`).prop('checked', checked);
    return this;
  }

  checkedItems(checked: boolean): DataTablesHelper {
    $(".dataTable input[type='checkbox']").prop('checked', checked);
    return this;
  }

  groupSelectedData(field: string): any {
    if (this.hasSelectedData()) {
      return _.groupBy(this.getSelectedData(), field);
    }
    return null;
  }

  configFilters(datatableElement: DataTableDirective): DataTablesHelper {
    this.applyStoredFilters();
    this.layzyDatatableFilters(datatableElement);
    return this;
  }

  layzyDatatableFilters(datatableElement: DataTableDirective): DataTablesHelper{
    setTimeout(() =>{
      this.applyFiltersDatatable(datatableElement);
    }, 1000)
    return this;
  }

  applyStoredFilters(): DataTablesHelper {
    if (this.hasStoreFilters()) {
      const storedFilters = this.getStoredFilters();
      this.setFormFiltersValues(storedFilters);
      this.loadFilters = true;
      this.removeStoredFilters();
    }
    return this;
  }

  applyFilters(datatableElement: DataTableDirective): DataTablesHelper {
    return this.applyFiltersForm().applyFiltersDatatable(datatableElement);
  }

  applyFiltersDatatable(datatableElement: DataTableDirective): DataTablesHelper {
    const instance = this;
    datatableElement.dtInstance.then(function (dtInstance: DataTables.Api) {
      const that = dtInstance;
      dtInstance.columns().every(function () {
        const columnName = this.dataSrc();
        if(columnName){
          const value = instance.getFormFilterValue(columnName.toString());
          this.search(value);
        }
      });
      that.draw();
    });
    return this;
  }

  setFormFiltersValues(filtersValues: any): DataTablesHelper {
    if (filtersValues) {
      const columnsSearch = this.getColumnsFilters();
      if (columnsSearch) {
        columnsSearch.forEach(column => {
          const columnName = column['data'];
          const columnValue = filtersValues[columnName];
          if (columnValue) {
            this.setFormFilterValue(columnName, columnValue);
          }
        });
      }
    }
    return this;
  }

  getFormFilterValue(filterColumn: string): any {
    return $(`#search-${filterColumn}-filter`).val();
  }

  setFormFilterValue(filterColumn: string, columnValue: any, filterValue?: any): DataTablesHelper {
    if (!filterValue) {
      filterValue = columnValue;
    }
    $(`#search-${filterColumn}-filter`).val(filterValue);
    return this;
  }

  applyFiltersForm(): DataTablesHelper {
    const columns: Array<any> = this.getColumnsFilters();
    columns.forEach(column => {
      const value = this.getFormFilterValue(column.data);
      $(`#search-${column.data}`).val(value);
    })
    return this;
  }

  applyClearFiltersForm(): DataTablesHelper {
    const columns: Array<any> = this.getColumnsFilters();
    columns.forEach(column => {
      $(`#search-${column.data}-filter`).val("");
      $(`#search-${column.data}`).val("");
    })
    return this;
  }

}
