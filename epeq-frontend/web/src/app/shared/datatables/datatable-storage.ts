import { DateUtils } from "../../core/utils/date-utils";

export class DataTableStorage {

  static setValue(key: string, value: string) {
    localStorage.setItem(key.toLowerCase(), value);
  }

  static getValue(key: string): string {
    return localStorage.getItem(key.toLowerCase());
  }

  static setObject(key: string, value: any) {
    DataTableStorage.setValue(key, JSON.stringify(value));
  }

  static getObject(key: string): any {
    let value = DataTableStorage.getValue(key);
    return JSON.parse(value);
  }

  static removeItem(key: string){
    localStorage.removeItem(key);
  }

  clear(){
    localStorage.clear();
  }

}

export class DataTableStoreFilters{

  filters = {}
  name: string;

  constructor(name: string){
    this.name = name;
  }

  static newFilter(filterName: string): DataTableStoreFilters{
    return new DataTableStoreFilters(filterName);
  }

  set(key: string, value: string): DataTableStoreFilters{
    this.filters[key] = value;
    return this;
  }

  setPeriodo(key: string, periodo: string, intervalo: string): DataTableStoreFilters{
    if(intervalo && intervalo !== ''){
      this.set(key, intervalo);

    }else if(periodo && periodo !== ''){
      this.set(key, DateUtils.formatPeriodoToStr(periodo))
    }

    return this;
  }

  store(): void{
    DataTableStorage.setObject(this.name, this.filters);
  }


}
