import { Component, Input, forwardRef, HostBinding } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-select-periodos',
  templateUrl: './select-periodos.component.html',
  styleUrls: ['./select-periodos.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectPeriodosComponent),
      multi: true
    }
  ]
})
export class SelectPeriodosComponent implements ControlValueAccessor {

  @HostBinding('attr.id')
  externalId = '';

  @Input('label')
  label = 'Período'

  @Input()
  set id(value: string) {
    this._ID = value;
    this.externalId = null;
  }

  @Input('value') _value = { tipo: '', valor: '' };

  onChange: any = () => { };

  onTouched: any = () => { };

  private _ID = '';

  private showIntervalo = false;

  constructor() { }

  get id() {
    return this._ID;
  }

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  selectOpcaoPeriodo(e) {
    this.value['tipo'] = e.target.value;
    this.showIntervalo = (this.value['tipo'] === "INTERVALO");
  }

  changeIntervaloPerioco(e) {
    this.value['valor'] = e.target.value;
  }

}
