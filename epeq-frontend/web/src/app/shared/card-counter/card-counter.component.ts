import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-counter',
  templateUrl: './card-counter.component.html',
  styleUrls: ['./card-counter.component.css']
})
export class CardCounterComponent implements OnInit {

  @Input()
  counterName: string;

  @Input()
  counterIcon: string;

  @Input()
  counterValue: string;

  @Input()
  cardColor: string;

  constructor() { }

  ngOnInit() {
  }

}
